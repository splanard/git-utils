#!/bin/bash
git config --global alias.reset-origin '!git reset --hard origin/$(git rev-parse --abbrev-ref HEAD)'